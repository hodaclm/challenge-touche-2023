import csv
import sys

languages_to_analyse = ('en', 'fr', 'es')

lan_name_dict = {
    "en":"Anglais",
    "de":"Allemand",
    "fr":"Français",
    "it":"Italien",
    "es":"Espagnol",
    "hu":"Hongrois",
    "nl":"Néerlandais",
    "fi":"Finnois",
    "pl":"Polonais",
    "el":"Grec",
    "sv":"Suédois",
    "cs":"Tchèque",
    "ca":"Catalan",
    "pt":"Portugais",
    "sk":"Slovaque",
    "eo":"Espéranto",
    "lv":"Letton",
    "ro":"Roumain",
    "lt":"Lituanien",
    "hr":"Croate",
    "bg":"Bulgare",
    "et":"Estonien",
    "da":"Danois",
    "sl":"Slovène",
    "ga":"Irlandais"
}

lan_counter_cfs = {}
lan_counter_cfu = {}
lan_counter_cfed = {}
topic_counter_cfs = {}

with open(f"datasets/CFS.tsv") as cfs, open(f"datasets/CFU.tsv") as cfu, open(f"datasets/CFE-D.tsv") as cfed:
    reader_cfs = csv.DictReader(cfs, delimiter='\t', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    reader_cfu = csv.DictReader(cfu, delimiter='\t', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    reader_cfed = csv.DictReader(cfed, delimiter='\t', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    for row_s in reader_cfs:
        if row_s["lan"] in lan_counter_cfs:
            if row_s["alignment"] == "In favor":
                lan_counter_cfs[row_s["lan"]][0] += 1
            elif row_s["alignment"] == "Against":
                lan_counter_cfs[row_s["lan"]][1] += 1
        else:
            if row_s["alignment"] == "In favor":
                lan_counter_cfs[row_s["lan"]] = [1,0]
            elif row_s["alignment"] == "Against":
                lan_counter_cfs[row_s["lan"]] = [0,1]

        if row_s["Topic"] in topic_counter_cfs:
            if row_s["lan"] in topic_counter_cfs[row_s["Topic"]]:
                topic_counter_cfs[row_s["Topic"]][row_s["lan"]][row_s["alignment"]] += 1
            else:
                topic_counter_cfs[row_s["Topic"]][row_s["lan"]] = {"In favor":0,"Against":0}
                topic_counter_cfs[row_s["Topic"]][row_s["lan"]][row_s["alignment"]] += 1
        else:
            topic_counter_cfs[row_s["Topic"]] = {row_s["lan"]:{"In favor":0,"Against":0}}
            topic_counter_cfs[row_s["Topic"]][row_s["lan"]][row_s["alignment"]] += 1



    for row_u in reader_cfu:
        if row_u["lan"] in lan_counter_cfu:
            lan_counter_cfu[row_u["lan"]] += 1
        else:
            lan_counter_cfu[row_u["lan"]] = 1

    for row_ed in reader_cfed:
        if row_ed["lan"] in lan_counter_cfed:
            if row_ed["label"] == "In favor":
                lan_counter_cfed[row_ed["lan"]][0] += 1
            elif row_ed["label"] == "Against":
                lan_counter_cfed[row_ed["lan"]][1] += 1
            elif row_ed["label"] == "Other":
                lan_counter_cfed[row_ed["lan"]][2] += 1
        else:
            if row_ed["label"] == "In favor":
                lan_counter_cfed[row_ed["lan"]] = [1,0,0]
            elif row_ed["label"] == "Against":
                lan_counter_cfed[row_ed["lan"]] = [0,1,0]
            elif row_ed["label"] == "Other":
                lan_counter_cfed[row_ed["lan"]] = [0,0,1]

sort_lang = sorted(lan_counter_cfs.items(), key=lambda x:sum(x[1]), reverse=True)
sum_cfs = sum([sum(r[1]) for r in lan_counter_cfs.items()])
sum_cfu = sum([r[1] for r in lan_counter_cfu.items()])
sum_cfed = sum([sum(r[1]) for r in lan_counter_cfed.items()])

for lang in sort_lang:
    if lang[0] in languages_to_analyse:
        print(f"{lan_name_dict[lang[0]]} ({lang[0]}):\n")

        # Affichage du tableau général
        print(f"|Dataset|Nombre de commentaires en {lan_name_dict[lang[0]].lower()}|Pourcentage par rapport au dataset|Pour (et %)|Contre (et %)|Neutre (et %)|")
        print("|---|---|---|---|---|---|")
        print(f"|CFs|{sum(lang[1])}|{(sum(lang[1])*100)/sum_cfs:.2f}%|{lang[1][0]} ({(lang[1][0]*100)/sum(lang[1]):.2f}%)|{lang[1][1]} ({(lang[1][1]*100)/sum(lang[1]):.2f}%)|X|")
        print(f"|CFu|{lan_counter_cfu[lang[0]]}|{(lan_counter_cfu[lang[0]]*100)/sum_cfu:.2f}%|X|X|X|")
        if lang[0] in lan_counter_cfed.keys():
            print(f"|CFe-d|{sum(lan_counter_cfed[lang[0]])}|{(sum(lan_counter_cfed[lang[0]])*100)/sum_cfed:.2f}%|{lan_counter_cfed[lang[0]][0]} ({(lan_counter_cfed[lang[0]][0]*100)/sum(lan_counter_cfed[lang[0]]):.2f}%)|{lan_counter_cfed[lang[0]][1]} ({(lan_counter_cfed[lang[0]][1]*100)/sum(lan_counter_cfed[lang[0]]):.2f}%)|{lan_counter_cfed[lang[0]][2]} ({(lan_counter_cfed[lang[0]][2]*100)/sum(lan_counter_cfed[lang[0]]):.2f}%)|")
        else:
            print("|CFe-d|---|---|---|---|---|")
        print()

        # Affichage du tableau CFs
        print("CFs")
        print(f"|Topic|Nombre de commentaires en {lan_name_dict[lang[0]].lower()}|Pourcentage par rapport au dataset|Pour (et %)|Contre (et %)|")
        print("|---|---|---|---|---|")
        for i in topic_counter_cfs.items():
            print(f"|{i[0]}|{sum(i[1][lang[0]].values())}|{(sum(i[1][lang[0]].values())*100)/sum(lang[1]):.2f}%|{i[1][lang[0]]['In favor']} ({(i[1][lang[0]]['In favor']*100)/sum(i[1][lang[0]].values()):.2f}%)|{i[1][lang[0]]['Against']} ({(i[1][lang[0]]['Against']*100)/sum(i[1][lang[0]].values()):.2f}%)|")
        print("_"*40)
        print()