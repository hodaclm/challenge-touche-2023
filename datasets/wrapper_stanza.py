# -*- coding: utf-8 -*-

import os


def parser(text, lang, out=None, separateur="\t\t"):
    """

    :param lang: langue du texte, défaut fr
    :param separateur: séparateur du flux d'entrée. Si non précisé, tout le texte est passé en mémoire. Si c'est une chaine, elle doit correspondre au contenu d'une ligne entière. Indiquer \"\" pour segmenter sur les lignes vides. Les fichiers de sortie seront numérotés en ajoutant -N à la fin du nom
    :param out: fichier de sortie, par défaut stanza-XX.conllu (XX correspond au numéro de processus).
    :return:
    """
    try:
        import stanza
        from stanza.utils.conll import CoNLL
    except ImportError:
        print ("""Stanza n'est pas installé (ou pas complètement). Pour ce faire, lancer dans un terminal :
            python3 -m pip install stanza
            """)
        exit()

    try:
        nlpStanza = stanza.Pipeline(lang)
    except BaseException:
        print(f"""Erreur de la part de Stanza : la langue {lang} n'est pas installée. Pour ce faire, lancer :
        python3 -c 'import stanza;stanza.download(\"{lang}\")'
        """)
        exit()

    if out is None:
        outputFileBase="stanza-"+str(os.getpid())+".conllu"
    else:
        outputFileBase=out

    if separateur is None:
        inputText = text
        output = nlpStanza(inputText)
        CoNLL.write_doc2conll(output,outputFileBase)

    else:
        inputText=""
        N=0
        for i in range(0, len(text)):
            line = text[i].rstrip("\n")
            if line == separateur and inputText:
                output = nlpStanza(inputText)
                CoNLL.write_doc2conll(output, out[N]+".conllu")
                inputText=""
                N+=1
            else:
                inputText += line+"\n"

        if inputText:
            output = nlpStanza(inputText)
            CoNLL.write_doc2conll(output, out[N]+".conllu")
            N += 1
            inputText=""
