import re
import os


def genre_determinant_analyse(path):
    """
    Analyse all the determinants from a conllu parsed file
    :param path: path from a file with text parsed in a conllu format
    :return: a dict with the frequency of the determinants distributions
    """
    conllu_file = open(path, mode="r")
    words = []
    tags = []
    morph = []
    gender_freq = dict()
    number_freq = dict()
    freq = dict()
    for line in conllu_file:
        line = line.rstrip("\n")
        split_line = line.split("\t")
        if len(split_line) >= 10:
            words.append(split_line[1])
            tags.append(split_line[3])
            morph.append(split_line[5])
        elif len(words) >= 1:
            for i in range(1, len(words)):
                gender = re.search(r"Gender=(\w+)", morph[i])
                number = re.search(r"Number=(\w+)", morph[i])
                if tags[i] == "DET" and gender:
                    gender_freq[gender.group(1)] = (
                        gender_freq.get(gender.group(1), 0) + 1
                    )
                if tags[i] == "DET" and number:
                    number_freq[number.group(1)] = (
                        number_freq.get(number.group(1), 0) + 1
                    )
                    # print(gender.group(1))
                    # freq[morph[i]] = freq.get(morph[i], 0) + 1
                    # print(words[i-1], tags[i-1], gouv_relation[i-1], ":", words[i], tags[i], ind[i])
            words = []
            tags = []
            morph = []
    conllu_file.close()
    s = sum(number_freq.values())
    for k, v in number_freq.items():
        pct = v * 100.0 / s
        number_freq[k] = str(v) + " (" + str(pct) + "%)"
    s = sum(gender_freq.values())
    for k, v in gender_freq.items():
        pct = v * 100.0 / s
        gender_freq[k] = str(v) + " (" + str(pct) + "%)"
    freq["number"] = number_freq
    freq["gender"] = gender_freq
    return freq


def resume_alignement(files_path):
    """
    Gets a resume from the determinant analyse in a list of files according the comment alignment
    :param files_path: (lst) a list with file paths to apply the determinant analyse
    :return: (dict) a dict with the resume frequency
    """
    resume = dict()
    for path in files_path:
        determinant_analyse = genre_determinant_analyse(path)
        resume[path] = determinant_analyse
    return resume


def batch_determinants_analyze():
    """
    applies the determinants analyze and print the result
    """
    basepath = "analysed_data/"
    analyzed_files_paths = []
    for entry in os.listdir(basepath):
        if os.path.isfile(os.path.join(basepath, entry)):
            analyzed_files_paths.append(os.path.join(basepath, entry))
    freq = resume_alignement(analyzed_files_paths)

    for file in freq:
        print(file, freq[file], sep="\t")


# determinants gender and number analyse
batch_determinants_analyze()
