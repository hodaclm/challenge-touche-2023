import sys
import pandas as pd

data = pd.read_csv(sys.stdin, sep="\t")


def data_by_column(column_name):
    """
    :param column_name: the name of the column wanted
    :return: a list with all the values in the column_name data
    """
    try:
        return list(data[column_name])
    except:
        print(f"It seems that this column name {column_name} does not exist")


def exclusive_voc(column_name):
    """
    :param column_name: the name of the column wanted
    :return: a list with all the words part of another columns, but not in the column_name.
    """
    lemma = data_by_column(data.columns[0])
    interested_column = data_by_column(column_name)
    if interested_column is not None:
        not_present = []
        for i in range(0, len(lemma)):
            if int(interested_column[i]) < 1:
                not_present.append(lemma[i])
        return not_present
    else:
        print(f"No data available for this {column_name}")


def specific_column_voc(column_name):
    """
    :param column_name: the name of the column wanted
    :return: a list with all the words not part of another columns.
    """
    lemma = data_by_column(data.columns[0])
    interested_column = data_by_column(column_name)
    col2 = " ".join([col for col in data.columns[2:] if col != column_name])
    another_column = data_by_column(col2)
    if interested_column is not None:
        specific_lemma = []
        for i in range(0, len(lemma)):
            if int(interested_column[i]) > 1 and int(another_column[i])<1:
                specific_lemma.append(lemma[i])
        index_max = max(range(len(interested_column)), key=interested_column.__getitem__)
        freq = lemma[index_max]
        return specific_lemma, freq
    else:
        return None

def rel_freq(x):
    freqs = [(value, x.count(value) / len(x)) for value in set(x)]
    return freqs


def index_summary(column):
    voc_not_present = exclusive_voc(column)
    specific_voc = specific_column_voc(column)
    # freq_rel_in_column = rel_freq(list(data[column]))
    if voc_not_present is not None:
        print(f"Lemmas not present in {column} examples")
        print("\n".join(voc_not_present))
        print("\n")
    else:
        pass
    if specific_voc is not None:
        print(f"Lemmas present only in {column} examples")
        print("\n".join(specific_voc[0]))
        print("The element more frequent is", specific_voc[1])
    # print(freq_rel_in_column)

resultats = index_summary("infavor")
