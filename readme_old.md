# Intra-Multilingual Multi-Target Stance Classification 2023

![Beatrix Kiddo M1 LITL@Touché 2023](https://drive.google.com/uc?export=view&id=1VJcRv3VH7edmcn3USXUlMXt-fVQ_p2aD)

* [Présentation du projet](#pr%C3%A9sentation-du-projet)
* [Données](#donn%C3%A9es)
    * [Présentation des données](#pr%C3%A9sentation-des-donn%C3%A9es)
    * [Aperçu quantitatif des données](#aper%C3%A7u-quantitatif-des-donn%C3%A9es)
        * [Dataset CFS](#dataset-cfs)

## Présentation du projet

Le projet de cette année fait partie de [Touché](https://touche.webis.de/clef23/touche23-web/index.html "Touché et CLEF 2023"), qui est une campagne d'événements
scientifiques portant sur des différentes tâches reliées à l’argumentation. Plus précisément,
nous allons nous concentrer sur la [tâche 4](https://touche.webis.de/clef23/touche23-web/multilingual-stance-classification.html "Intra-Multilingual Multi-Target Stance Classification 2023"). L’objectif de cette tâche est de créer un système
permettant de classer des commentaires en trois catégories: **pour**, **contre** ou **neutre**. Il s’agit
de commentaires postés en réaction à une proposition portant sur les sujets suivants:
* Le climat et l’environnement
* La santé
* L’économie
* L’EU dans le monde
* Les valeurs et les droits, la loi et la sécurité
* La transformation digitale
* La démocratie Européenne
* La migration
* L’éducation, la culture, la jeunesse et le sport
* Des idées autres

Les propositions et les commentaires peuvent être écrits en 26 langues, les 24 langues
européennes plus l’espéranto et le catalan. Aussi, ils sont accompagnés de leur traduction
automatique en anglais. Le jeu de données comprend 4,000 propositions et 20,000
commentaires y répondant.

[La tâche est accessible ici](https://touche.webis.de/clef23/touche23-web/multilingual-stance-classification.html "Intra-Multilingual Multi-Target Stance Classification 2023")

## Données

### Présentation des données

Les [données](https://touche.webis.de/clef23/touche23-web/multilingual-stance-classification.html#data "Données pour la tâche 4") pour la tâche proviennent de [CoFE](https://drive.google.com/file/d/1D3ao_A3bKx-Xd1JK0c8t8Uv_x6NSGU8N/view "Présentation des données CoFE") (Conference on the Future of Europe), une
plateforme européenne de Participation Démocratique. Les données se divisent en trois
sous-groupes:
* **CFs**: 7,000 commentaires classés en pour ou contre par leur propre auteur.
* **CFu**: commentaires sans annotations
* **CFe**: 1,200 commentaires classés par INCEpTION en pour, contre ou neutre. Nous y
trouvons des commentaires seulement en français, allemand, anglais, grec, italien et
hongrois.

Notre objectif sera de mener, en groupe, une étude sur les éléments linguistiques
d’argumentations des commentaires postés. Pour cela, nous utiliserons les jeux de données
mis à notre disposition afin d’observer des récurrences. L’objectif étant de pouvoir prédire à
partir de ces indices si le commentaire est pour, contre ou neutre. Riche de la maîtrise de
plusieurs langues par le groupe, nous pourrons étudier les arguments des commentaires de
différentes langues. Si le temps nous le permet, nous pourrons automatiser ces
observations et proposer des runs.

### Aperçu quantitatif des données

##### Dataset CFs
Nous dressons ici un tableau de la représentativité des commentaires par langue pour le jeu de données **CFs**.

|Langue|Code langue|Nombre de commentaires|Pourcentage de représentativité|
|------|-----------|----------------------|-------------------------------|
|Anglais|en|3192|45.59%|
|Allemand|de|1379|19.69%|
|Français|fr|865|12.35%|
|Italien|it|352|5.03%|
|Espagnol|es|308|4.40%|
|Hongrois|hu|284|4.06%|
|Néerlandais|nl|140|2.00%|
|Finnois|fi|56|0.80%|
|Polonais|pl|54|0.77%|
|Grec|el|52|0.74%|
|Suédois|sv|44|0.63%|
|Tchèque|cs|39|0.56%|
|Catalan|ca|39|0.56%|
|Portugais|pt|36|0.51%|
|Slovaque|sk|25|0.36%|
|Espéranto|eo|20|0.29%|
|Letton|lv|20|0.29%|
|Roumain|ro|19|0.27%|
|Lituanien|lt|16|0.23%|
|Croate|hr|15|0.21%|
|Bulgare|bg|14|0.20%|
|Estonien|et|14|0.20%|
|Danois|da|14|0.20%|
|Slovène|sl|3|0.04%|
|Irlandais|ga|2|0.03%|

Voici la répartition des stances selon les langues que nous avons choisi d'étudier :

|Langue|Code langue|Nombre de commentaires|Nombre de pour|Nombre de contre|
|------|-----------|----------------------|--------------|----------------|
|Anglais|en|3192|2333|859|
|Allemand|de|1379|1111|268|
|Français|fr|865|703|162|
|Italien|it|352|296|56|
|Espagnol|es|308|276|32|
|Néerlandais|nl|140|113|27|
|Polonais|pl|54|44|10|
|Roumain|ro|19|19|0|
|Croate|hr|15|15|0|

![UT2J](https://authc.univ-toulouse.fr/assets/logos/ut2-a933aeba56cf90831cd94e724d1d3982.png)