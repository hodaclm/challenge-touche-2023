import csv
import sys
import os
import re

folder_name = "datasets"

# Déclaration des langues à compiler
languages = ("fr", "en", "es")
data = {}

# Initialisation du dico data
for l in languages:
    data[l] = []

# Population du dico data avec le dataset CFS
reader = csv.DictReader(sys.stdin, delimiter='\t', quotechar='"', quoting=csv.QUOTE_MINIMAL)
for row in reader:
    if row["lan"] in languages:
        data[row["lan"]].append(row)

# Création de l'arborescence de dossiers
try:
    os.mkdir(folder_name)
    print(f"Répertoire {folder_name} créé")
except FileExistsError:
    print(f"Répertoire {folder_name} déjà existant")
try:
    os.mkdir(folder_name + "/CFS")
    print(f"Répertoire {folder_name + '/CFS'} créé")
except FileExistsError:
    print(f"Répertoire {folder_name + '/CFS'} déjà existant")

for lang in languages:
    try:
        os.mkdir(f"{folder_name + '/CFS'}/CFS_{lang}")
        print(f"Répertoire {folder_name + '/CFS'}/CFS_{lang} créé")
    except FileExistsError:
        print(f"Répertoire {folder_name + '/CFS'}/CFS_{lang} déjà existant")

# Création des fichiers tsv et alceste
for language in data:
    with open(f"datasets/CFS/CFS_{language}/CFS_{language}.tsv", mode="w") as file, open(f"datasets/CFS/CFS_{language}/CFS_{language}_infavor.tsv", mode="w") as file_infavor, open(f"datasets/CFS/CFS_{language}/CFS_{language}_against.tsv", mode="w") as file_against, open(f"datasets/CFS/CFS_{language}/CFS_{language}_alceste.txt", mode="w") as alceste:
        writer = csv.DictWriter(file, data[language][0].keys(), delimiter='\t', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        writer.writeheader()
        writer_infavor = csv.DictWriter(file_infavor, data[language][0].keys(), delimiter='\t', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        writer_infavor.writeheader()
        writer_against = csv.DictWriter(file_against, data[language][0].keys(), delimiter='\t', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        writer_against.writeheader()
        for row in data[language]:
            if row["id"] not in ("comment_58807", "comment_78444"):
                writer.writerow(row)
                comid = re.search(r'comment_(\d+)', row['id']).group(1)
                alignment = "".join(row["alignment"].lower().split(" "))
                topic = "".join(row["Topic"].split(" "))
                alceste.write(f"**** *comid_{comid} *alignment_{alignment} *topic_{topic}\n{row['comment']}\n\n")
                if row["alignment"] == "In favor":
                    writer_infavor.writerow(row)
                elif row["alignment"] == "Against":
                    writer_against.writerow(row)

    print(f"{language} completed")
