import re
import sys

vraisPositifs = 0
vraisNegatifs = 0
fauxPositifs = 0
fauxNegatifs = 0
score = 0

formes = []
#tag = []

for ligne in sys.stdin:
    ligne = ligne.rstrip("\n")
    
    if ligne.startswith("<comment_"):
        alignment_match = re.search(r"alignment='([^']*)'", ligne)
        if alignment_match:
            alignment = alignment_match.group(1)
            formes = []
            #tag = []
            score = 0

    l = ligne.split("\t")
    if len(l)==10:
        formes.append(l[1])
        tag.append(l[3])

        for i in range(0, len(formes)):
            if formes[i] =="?":
                score -= 1
            if formes[i] == "!":
                score += 1

    elif ligne.startswith("</comment_"):
        if score > 0 and alignment == "In favor":
            vraisPositifs += 1
        if score > 0 and alignment == "Against":
            fauxNegatifs += 1
        if score < 0 and alignment == "In favor":
            fauxPositifs += 1
        if score < 0 and alignment == "Against":
            vraisNegatifs += 1    

if vraisPositifs + fauxNegatifs > 0:
    rappelPositifs = vraisPositifs/(vraisPositifs+fauxNegatifs)

if vraisNegatifs + fauxPositifs > 0:
    rappelNegatifs = vraisNegatifs/(vraisNegatifs+fauxPositifs)


print("vrais positifs :", vraisPositifs)
print("vrais négatifs :", vraisNegatifs)
print("faux positifs :", fauxPositifs)
print("faux négatifs :", fauxNegatifs)
print("-" * 33)
print("précision positifs :", vraisPositifs / (vraisPositifs + fauxPositifs))
print("formule : vraisPositifs/(vraisPositifs+fauxPositifs)")
print("précision négatifs :", vraisNegatifs / (vraisNegatifs + fauxNegatifs))
print("formule : vraisNegatifs/(vraisNegatifs+fauxPositifs)")
print("-"*33)
print("rappel positifs :", rappelPositifs)
print("formule : vraisPositifs/(vraisPositifs+fauxNegatifs)")
print("rappel négatifs :", rappelNegatifs)
print("formule : vraisNegatifs/(vraisNegatifs+fauxPositifs)")