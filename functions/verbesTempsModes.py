import sys
import re

pos=[]
lemmes=[]
traits=[]
verbes=0
present1Pl=0
present2Pl=0
present3Pl=0
present1Sing=0
present2Sing=0
present3Sing=0
passe1Pl=0
passe2Pl=0
passe3Pl=0
passe1Sing=0
passe2Sing=0
passe3Sing=0
futur1Pl=0
futur2Pl=0
futur3Pl=0
futur1Sing=0
futur2Sing=0
futur3Sing=0
imp1Pl=0
imp2Pl=0
imp3Pl=0
imp1Sing=0
imp2Sing=0
imp3Sing=0
Spresent1Pl=0
Spresent2Pl=0
Spresent3Pl=0
Spresent1Sing=0
Spresent2Sing=0
Spresent3Sing=0
Spasse1Pl=0
Spasse2Pl=0
Spasse3Pl=0
Spasse1Sing=0
Spasse2Sing=0
Spasse3Sing=0
Simp1Pl=0
Simp2Pl=0
Simp3Pl=0
Simp1Sing=0
Simp2Sing=0
Simp3Sing=0

for line in sys.stdin:
	line = line.rstrip("\n")

	l = line.split("\t")
	if len(l) == 10:
		pos.append(l[3])
		lemmes.append(l[2])
		traits.append(l[5])

#Identifier POS
for i in range(0,len(pos)-1):
	if pos[i] == "AUX" or pos[i]=="VERB":
		verbes=verbes+1
		#INDICATIF
		#QUANTIFICATION DU PRÉSENT, personnes au pluriel
		if traits[i].startswith("Mood=Ind|Number=Plur|Person=1|Tense=Pres"):
			present1Pl=present1Pl+1
	
		elif traits[i].startswith("Mood=Ind|Number=Plur|Person=2|Tense=Pres"):
			present2Pl=present2Pl+1

		elif traits[i].startswith("Mood=Ind|Number=Plur|Person=3|Tense=Pres"):
			present3Pl=present3Pl+1

		#personnes au singulier
		elif traits[i].startswith("Mood=Ind|Number=Sing|Person=1|Tense=Pres"):
			present1Sing=present1Sing+1

		elif traits[i].startswith("Mood=Ind|Number=Sing|Person=2|Tense=Pres"):
			present2Sing=present2Sing+1
			
		elif traits[i].startswith("Mood=Ind|Number=Sing|Person=3|Tense=Pres"):
			present3Sing=present3Sing+1

		#QUANTIFICATION DU PASSÉ, personnes au pluriel
		if traits[i].startswith("Mood=Ind|Number=Plur|Person=1|Tense=Past"):
			passe1Pl=passe1Pl+1
	
		elif traits[i].startswith("Mood=Ind|Number=Plur|Person=2|Tense=Past"):
			passe2Pl=passe2Pl+1

		elif traits[i].startswith("Mood=Ind|Number=Plur|Person=3|Tense=Past"):
			passe3Pl=passe3Pl+1

		#personnes au singulier
		elif traits[i].startswith("Mood=Ind|Number=Sing|Person=1|Tense=Past"):
			passe1Sing=passe1Sing+1

		elif traits[i].startswith("Mood=Ind|Number=Sing|Person=2|Tense=Past"):
			passe2Sing=passe2Sing+1
			
		elif traits[i].startswith("Mood=Ind|Number=Sing|Person=3|Tense=Past"):
			passe3Sing=passe3Sing+1

		#QUANTIFICATION DU FUTUR, personnes au pluriel (anglais n'a pas Tense=Fut)
		if traits[i].startswith("Mood=Ind|Number=Plur|Person=1|Tense=Fut"):
			futur1Pl=futur1Pl+1
	
		elif traits[i].startswith("Mood=Ind|Number=Plur|Person=2|Tense=Fut"):
			futur2Pl=futur2Pl+1

		elif traits[i].startswith("Mood=Ind|Number=Plur|Person=3|Tense=Fut"):
			futur3Pl=futur3Pl+1

		#personnes au singulier
		elif traits[i].startswith("Mood=Ind|Number=Sing|Person=1|Tense=Fut"):
			futur1Sing=futur1Sing+1

		elif traits[i].startswith("Mood=Ind|Number=Sing|Person=2|Tense=Fut"):
			futur2Sing=futur2Sing+1
			
		elif traits[i].startswith("Mood=Ind|Number=Sing|Person=3|Tense=Fut"):
			futur3Sing=futur3Sing+1

		#QUANTIFICATION DE L'IMPARFAIT, personnes au pluriel (anglais n'a pas Tense=Imp)
		if traits[i].startswith("Mood=Ind|Number=Plur|Person=1|Tense=Imp"):
			imp1Pl=imp1Pl+1
	
		elif traits[i].startswith("Mood=Ind|Number=Plur|Person=2|Tense=Imp"):
			imp2Pl=imp2Pl+1

		elif traits[i].startswith("Mood=Ind|Number=Plur|Person=3|Tense=Imp"):
			imp3Pl=imp3Pl+1

		#personnes au singulier
		elif traits[i].startswith("Mood=Ind|Number=Sing|Person=1|Tense=Imp"):
			imp1Sing=imp1Sing+1

		elif traits[i].startswith("Mood=Ind|Number=Sing|Person=2|Tense=Imp"):
			imp2Sing=imp2Sing+1
			
		elif traits[i].startswith("Mood=Ind|Number=Sing|Person=3|Tense=Imp"):
			imp3Sing=imp3Sing+1

		#SUBJONCTIF
		#QUANTIFICATION DU PRÉSENT, personnes au pluriel
		if traits[i].startswith("Mood=Sub|Number=Plur|Person=1|Tense=Pres"):
			Spresent1Pl=Spresent1Pl+1
	
		elif traits[i].startswith("Mood=Sub|Number=Plur|Person=2|Tense=Pres"):
			Spresent2Pl=Spresent2Pl+1

		elif traits[i].startswith("Mood=Sub|Number=Plur|Person=3|Tense=Pres"):
			Spresent3Pl=Spresent3Pl+1

		#personnes au singulier
		elif traits[i].startswith("Mood=Sub|Number=Sing|Person=1|Tense=Pres"):
			Spresent1Sing=Spresent1Sing+1

		elif traits[i].startswith("Mood=Sub|Number=Sing|Person=2|Tense=Pres"):
			Spresent2Sing=Spresent2Sing+1
			
		elif traits[i].startswith("Mood=Sub|Number=Sing|Person=3|Tense=Pres"):
			Spresent3Sing=Spresent3Sing+1

		#QUANTIFICATION DU PASSÉ, personnes au pluriel
		if traits[i].startswith("Mood=Sub|Number=Plur|Person=1|Tense=Past"):
			Spasse1Pl=Spasse1Pl+1
	
		elif traits[i].startswith("Mood=Sub|Number=Plur|Person=2|Tense=Past"):
			Spasse2Pl=Spasse2Pl+1

		elif traits[i].startswith("Mood=Sub|Number=Plur|Person=3|Tense=Past"):
			Spasse3Pl=Spasse3Pl+1

		#personnes au singulier
		elif traits[i].startswith("Mood=Sub|Number=Sing|Person=1|Tense=Past"):
			Spasse1Sing=Spasse1Sing+1

		elif traits[i].startswith("Mood=Sub|Number=Sing|Person=2|Tense=Past"):
			Spasse2Sing=Spasse2Sing+1
			
		elif traits[i].startswith("Mood=Sub|Number=Sing|Person=3|Tense=Past"):
			Spasse3Sing=Spasse3Sing+1


		#QUANTIFICATION DE L'IMPARFAIT, personnes au pluriel (anglais n'a pas Tense=Imp)
		if traits[i].startswith("Mood=Sub|Number=Plur|Person=1|Tense=Imp"):
			Simp1Pl=Simp1Pl+1
	
		elif traits[i].startswith("Mood=Sub|Number=Plur|Person=2|Tense=Imp"):
			Simp2Pl=Simp2Pl+1

		elif traits[i].startswith("Mood=Sub|Number=Plur|Person=3|Tense=Imp"):
			Simp3Pl=Simp3Pl+1

		#personnes au singulier
		elif traits[i].startswith("Mood=Sub|Number=Sing|Person=1|Tense=Imp"):
			Simp1Sing=Simp1Sing+1

		elif traits[i].startswith("Mood=Sub|Number=Sing|Person=2|Tense=Imp"):
			Simp2Sing=Simp2Sing+1
			
		elif traits[i].startswith("Mood=Sub|Number=Sing|Person=3|Tense=Imp"):
			Simp3Sing=Simp3Sing+1


print("Indicatif")
print("---------------")
print("Présent, 1ere personne pluriel: ", present1Pl)
print("Présent, 2eme personne pluriel: ", present2Pl)
print("Présent, 3eme personne pluriel: ", present3Pl)
print("Présent, 1ere personne singulier: ", present1Sing)
print("Présent, 2eme personne singulier: ", present2Sing)
print("Présent, 3eme personne singulier: ", present3Sing)
print("Passé, 1ere personne pluriel: ", passe1Pl)
print("Passé, 2eme personne pluriel: ", passe2Pl)
print("Passé, 3eme personne pluriel: ", passe3Pl)
print("Passé, 1ere personne singulier: ", passe1Sing)
print("Passé, 2eme personne singulier: ", passe2Sing)
print("Passé, 3eme personne singulier: ", passe3Sing)
print("Futur, 1ere personne pluriel: ", futur1Pl)
print("Futur, 2eme personne pluriel: ", futur2Pl)
print("Futur, 3eme personne pluriel: ", futur3Pl)
print("Futur, 1ere personne singulier: ", futur1Sing)
print("Futur, 2eme personne singulier: ", futur2Sing)
print("Futur, 3eme personne singulier: ", futur3Sing)
print("Imparfait, 1ere personne pluriel: ", imp1Pl)
print("Imparfait, 2eme personne pluriel: ", imp2Pl)
print("Imparfait, 3eme personne pluriel: ", imp3Pl)
print("Imparfait, 1ere personne singulier: ", imp1Sing)
print("Imparfait, 2eme personne singulier: ", imp2Sing)
print("Imparfait, 3eme personne singulier: ", imp3Sing)
print("Subjonctif")
print("---------------")
print("Présent, 1ere personne pluriel: ", Spresent1Pl)
print("Présent, 2eme personne pluriel: ", Spresent2Pl)
print("Présent, 3eme personne pluriel: ", Spresent3Pl)
print("Présent, 1ere personne singulier: ", Spresent1Sing)
print("Présent, 2eme personne singulier: ", Spresent2Sing)
print("Présent, 3eme personne singulier: ", Spresent3Sing)
print("Passé, 1ere personne pluriel: ", Spasse1Pl)
print("Passé, 2eme personne pluriel: ", Spasse2Pl)
print("Passé, 3eme personne pluriel: ", Spasse3Pl)
print("Passé, 1ere personne singulier: ", Spasse1Sing)
print("Passé, 2eme personne singulier: ", Spasse2Sing)
print("Passé, 3eme personne singulier: ", Spasse3Sing)
print("Imparfait, 1ere personne pluriel: ", Simp1Pl)
print("Imparfait, 2eme personne pluriel: ", Simp2Pl)
print("Imparfait, 3eme personne pluriel: ", Simp3Pl)
print("Imparfait, 1ere personne singulier: ", Simp1Sing)
print("Imparfait, 2eme personne singulier: ", Simp2Sing)
print("Imparfait, 3eme personne singulier: ", Simp3Sing)